﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibMaestrosAsignatura;

namespace CrudMaestrosAsignaturas
{
    public partial class FrmMaestros : Form
    {
        //creo una instancia de MAestro
        Maestro maestroParaForm = new Maestro();
        //id de maestro de DataGrid
        int idSeleccionado;

        public FrmMaestros()
        {
            InitializeComponent();
        }

        private void FrmMaestros_Load(object sender, EventArgs e)
        {
            cargarMaestrosADataGrid();
        }

        private void cargarMaestrosADataGrid()
        {
            dgMaestros.DataSource = null;
            //borramos todos los renglones
            dgMaestros.Rows.Clear();

            //cargar TODOS los maestros registrados
            dgMaestros.DataSource = maestroParaForm.consultaVariosMaestros("", "", "@");
            //refrescar el DG
            dgMaestros.Refresh();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //validamos que la ontraeña si este confirmada correctamente
            if (txtContraseña.Text == txtConfirmaContra.Text)
            {

                //metemos todos los datos de los textBoxes en obj MAestro.
                maestroParaForm.Nombre = txtNombre.Text;
                maestroParaForm.Apellido = txtApellidos.Text;
                maestroParaForm.Correo = txtCorreo.Text;
                maestroParaForm.Celular = txtCel.Text;
                maestroParaForm.Contraseña = txtContraseña.Text;
                //ejecutamos alta
                if (maestroParaForm.alta())
                {
                    MessageBox.Show("Éxito, registraste al Profe", "Alta Maestro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //limpiar el form
                    txtApellidos.Text = txtCel.Text= txtConfirmaContra.Text=
                        txtContraseña.Text=txtCorreo.Text=txtNombre.Text="";
                    //refrescar el DG
                    cargarMaestrosADataGrid();
                }
                else
                {
                    MessageBox.Show(Maestro.MensajeDeError, "Error en lata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Oyes weys, esta mal esta madre...");
            }

        }

        private void dgMaestros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idSeleccionado = int.Parse(dgMaestros.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgMaestros.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApellidos.Text = dgMaestros.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtCorreo.Text = dgMaestros.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtCel.Text = dgMaestros.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtContraseña.Text = dgMaestros.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //ejecutamos mnodificar
            if (maestroParaForm.modificar(txtNombre.Text, txtApellidos.Text, txtCorreo.Text, txtCel.Text, txtContraseña.Text, idSeleccionado))
            {
                MessageBox.Show("Éxito, modificaste al Profe", "Modificar Maestro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //limpiar el form
                txtApellidos.Text = txtCel.Text = txtConfirmaContra.Text =
                    txtContraseña.Text = txtCorreo.Text = txtNombre.Text = "";
                //refrescar el DG
                cargarMaestrosADataGrid();
            }
            else
            {
                MessageBox.Show(Maestro.MensajeDeError, "Error en modificar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿De verdad quieres borrar a '"+txtNombre.Text+"'?", "Borrar Profe", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (maestroParaForm.borrar(idSeleccionado))
                {
                    MessageBox.Show("Éxito, borraste al Profe", "Borrar Maestro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //limpiar el form
                    txtApellidos.Text = txtCel.Text = txtConfirmaContra.Text =
                        txtContraseña.Text = txtCorreo.Text = txtNombre.Text = "";
                    //refrescar el DG
                    cargarMaestrosADataGrid();
                }
                else
                {
                    MessageBox.Show(Maestro.MensajeDeError, "Error en Borrar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
