﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace LibMaestrosAsignatura
{
    public class Maestro
    {
        //vars de BD
        MySqlConnection con;
        MySqlCommand com;
        MySqlDataReader dr;

        //var de errores
        public static string MensajeDeError;

        //constructor de Maestro
        public Maestro()
        {
            //se instancia BD... config
            con = new MySqlConnection("Server=10.0.2.2;Port=8889;Database=crud_maestros_asignaturas;Uid=root;Pwd=root;");
        }

        //propiedaes de objeto
        private int id;
        private string nombre;
        private string apellido;
        private string correo;
        private string celular;
        private string contraseña;
       
        //getters y Setters
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Celular { get => celular; set => celular = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }

        //acciones de Maestro
        public bool alta()
        {
            bool res = false;
            //hacer la progrmacion del alta en la BD
            try
            {
                //abrir conex
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                //ESTABLECER EL QUERY que quiero ejecutar
                //INSERT nombreTamble (campos1, campos2, campo3, campoN) VALUES (this.Nombre, val2, val3, valN)
                string query = "INSERT INTO maestros (nombre,       apellido    ,    correo       ,     celular       , contrasenia) " +
                                  "VALUES ('" + this.nombre + "','" + this.apellido + "','" + this.correo + "', '" + this.celular + "', '" + this.contraseña + "')";
                //comand
                com = new MySqlCommand(query, con);
                //EJECUTARLO
                com.ExecuteNonQuery();//sirve para insert, delete, update
                //esta listo :)
                res= true;
            }
            catch (MySqlException mysqlex)
            {
                Maestro.MensajeDeError = "Error SYNTAXIS en el insert." + mysqlex.Message;
            }
            catch (Exception ex)
            {
                Maestro.MensajeDeError = "Error GENERAL en el insert." + ex.Message;
            }
            finally {
                con.Close();
            }
            return res;
        }


        public List<Maestro> consultaVariosMaestros(string nombre, string apellido, string correo)
        {
            List<Maestro> resultado = new List<Maestro>();
            //hacer la consulta en la BD
            try
            {
                //validar conexion
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                //creo que l query
                string query = "SELECT * FROM maestros WHERE nombre LIKE '%"+nombre+"%' OR apellido LIKE '%"+apellido+"%' OR correo LIKE '%"+correo+"%'";
                //command
                com = new MySqlCommand(query, con);
                //executar la consultar
                dr = com.ExecuteReader();
                //validar alugn resultado
                if (dr.HasRows)
                {
                    //ciclo while para leer todos los resultados
                    while (dr.Read())
                    {
                        //creo obj maestro SIN CONSTURCTOR!!!!!!!
                        Maestro tmp = new Maestro
                        {
                            //mapeamos todas las variables a los props de MAESTRO
                            Id = int.Parse(dr[0].ToString()),
                            Nombre = dr[1].ToString(),
                            Apellido = dr[2].ToString(),
                            Correo = dr[3].ToString(),
                            Celular = dr[4].ToString(),
                            Contraseña = dr[5].ToString()
                        };
                        //agrego TMP a la lista RESULTADO
                        resultado.Add(tmp);
                    }
                }
                else
                {
                    resultado=null;
                }
            }
            catch (MySqlException mysqlex)
            {
                Maestro.MensajeDeError = "ERROR DE SYNTAXIS al consultar "+mysqlex.Message;
            }
            catch (Exception ex)
            {
                Maestro.MensajeDeError = "ERROR GENERAL al consultar " + ex.Message;
            }
            finally
            {
                con.Close();
            }
            return resultado;
        }

        //acciones de Maestro
        public bool modificar(string nombre, string ape, string correo, string cel, string contra, int id)
        {
            bool res = false;
            //hacer la progrmacion del alta en la BD
            try
            {
                //abrir conex
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                //ESTABLECER EL QUERY que quiero ejecutar
                //UPDATE tabla SET campo1=valor1, campo2=valor2, campoN=valorN WHERE id=X
                string query = "UPDATE maestros SET nombre='" + nombre + "',apellido='" + ape + "' "+
                                ",correo='" + correo + "', celular='" + cel + "', contrasenia='"+contra+"' WHERE id="+id ;
                //comand
                com = new MySqlCommand(query, con);
                //EJECUTARLO
                com.ExecuteNonQuery();//sirve para insert, delete, update
                //esta listo :)
                res = true;
            }
            catch (MySqlException mysqlex)
            {
                Maestro.MensajeDeError = "Error SYNTAXIS en el modificar." + mysqlex.Message;
            }
            catch (Exception ex)
            {
                Maestro.MensajeDeError = "Error GENERAL en el modificar." + ex.Message;
            }
            finally
            {
                con.Close();
            }
            return res;
        }

        //acciones de Maestro
        public bool borrar( int id)
        {
            bool res = false;
            //hacer la progrmacion del alta en la BD
            try
            {
                //abrir conex
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                //ESTABLECER EL QUERY que quiero ejecutar
                //DELETE FROM tabla WHERE id=X
                string query = "DELETE FROM maestros  WHERE id=" + id;
                //comand
                com = new MySqlCommand(query, con);
                //EJECUTARLO
                com.ExecuteNonQuery();//sirve para insert, delete, update
                //esta listo :)
                res = true;
            }
            catch (MySqlException mysqlex)
            {
                Maestro.MensajeDeError = "Error SYNTAXIS en el borrar." + mysqlex.Message;
            }
            catch (Exception ex)
            {
                Maestro.MensajeDeError = "Error GENERAL en el borrar." + ex.Message;
            }
            finally
            {
                con.Close();
            }
            return res;
        }

    }
}
